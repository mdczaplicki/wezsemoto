from crispy_forms.helper import FormHelper
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, ButtonHolder, Submit, HTML
from django import forms
from django.template.loader import render_to_string
from django.utils.translation import gettext as _
from phonenumber_field.formfields import PhoneNumberField

from main.models import Bike, BikePhoto, BikeOffer, BikeParameters
from main.utils import LabelFreeFormHelper


class BikeForm(forms.ModelForm):
    images = forms.ImageField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fields = [
            ('brand', _('Marka')),
            ('model', _('Model')),
            ('plate_number', _('Numer rejestracyjny')),
            ('date_of_production', _('Data produkcji')),
            ('images', _('Zdjęcia'))
        ]
        self.helper = LabelFreeFormHelper(_('Nowy motocykl'), fields, self)

    class Meta:
        model = Bike
        fields = ['brand', 'model', 'plate_number', 'date_of_production', 'images']
        widgets = {
            'date_of_production': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
        }

    def save_with_images(self, images, commit=True):
        new_bike = super().save(commit=False)
        if commit:
            new_bike.save()
        for image in images:
            bike_photo = BikePhoto.objects.create(photo=image, bike=new_bike)
            if commit:
                bike_photo.save()
        return new_bike


class BikeParametersForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fields = [
            ('name', _('Nazwa')),
            ('capacity', _('Pojemność')),
            ('power', _('Moc')),
            ('torque', _('Moment obrotowy')),
            ('tank_capacity', _('Pojemność baku')),
            ('seat_height', _('Wysokość siedzenia')),
            ('extra', _('Dodatkowe wyposażenie'))
        ]
        self.helper = LabelFreeFormHelper(_('Nowe parametry motocykla'), fields, self)

    class Meta:
        model = BikeParameters
        fields = ['name', 'capacity', 'power', 'torque', 'tank_capacity', 'seat_height', 'extra']


class EditBikeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fields = [
            ('name', _('Nazwa')),
            ('brand', _('Marka')),
            ('model', _('Model')),
            ('plate_number', _('Numer rejestracyjny')),
            ('date_of_production', _('Data produkcji')),
            ('short_description', _('Krótki opis')),
            ('description', _('Opis')),
            ('bike_parameters', _('Parametry'))
        ]
        self.helper = LabelFreeFormHelper(_('Edytuj motocykl'), fields, self)
        self.helper.layout.fields[0].fields.append(HTML(
            render_to_string('admin/cells/checkbox_button_group.html', {
                'yes_text': _('Widoczne'),
                'no_text': _('Niewidoczne'),
                'yes': 'active' if self.initial.get('is_visible') else '',
                'no': 'active' if not self.initial.get('is_visible') else '',
                'name': 'is_visible'
            })
        ))

    class Meta:
        model = Bike
        fields = ['name', 'brand', 'model', 'plate_number', 'date_of_production', 'short_description', 'description',
                  'bike_parameters', 'is_visible']
        widgets = {
            'date_of_production': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'short_description': forms.Textarea(attrs={'rows': 2})
        }


class EditBikePhotosForm(forms.ModelForm):
    images = forms.ImageField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.form_class = 'form-inline'
        self.helper.layout = Layout(
            Field('images'),
            ButtonHolder(
                Submit('add_photo', _('Dodaj zdjęcia'))
            )
        )

    class Meta:
        model = Bike
        fields = ['images']

    def save_with_images(self, images, commit=True):
        bike = self.instance
        for image in images:
            bike_photo = BikePhoto.objects.create(photo=image, bike=bike)
            if commit:
                bike_photo.save()
        return bike


class EditBikeOfferForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        # self.helper.form_class = ''
        self.helper.layout = Layout(
            Div(
                Field('duration', css_class='form-control w-100', placeholder=_('Okres')),
                css_class='form-group'
            ),
            Div(
                Field('price', css_class='form-control w-100', placeholder=_('Cena')),
                css_class='form-group'
            ),
            Submit('add_offer', _('Dodaj ofertę'), css_class='btn btn-primary w-100')
        )

    class Meta:
        model = BikeOffer
        fields = ['duration', 'price']

    def save(self, commit=True, bike=None):
        price = self.data.get('price')
        duration = self.data.get('duration')
        bike_offer = BikeOffer.objects.create(price=price, duration=duration, bike=bike)
        if commit:
            bike_offer.save()
        return bike_offer


class EditBikeParametersForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fields = [
            ('name', _('Nazwa')),
            ('capacity', _('Pojemność')),
            ('power', _('Moc')),
            ('torque', _('Moment obrotowy')),
            ('tank_capacity', _('Pojemnośc baku')),
            ('seat_height', _('Wysokość siedzenia')),
            ('extra', _('Dodatkowe wyposażenie'))
        ]
        self.helper = LabelFreeFormHelper(_('Edytuj motocykl'), fields, self)

    class Meta:
        model = BikeParameters
        fields = ['name', 'capacity', 'power', 'torque', 'tank_capacity', 'seat_height', 'extra']


class BikeContactForm(forms.Form):
    bike = forms.ModelChoiceField(queryset=Bike.objects.filter(is_visible=True), disabled=True, )
    mail = forms.EmailField()
    phone = PhoneNumberField()
    date_from = forms.DateField()
    date_to = forms.DateField()
    message = forms.CharField(widget=forms.Textarea(attrs={'rows': 2}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Div(
                Field('bike', css_class='form-control w-100', placeholder=_('Motocykl')),
                css_class='form-group'
            ),
            Div(
                Field('mail', css_class='form-control w-100', placeholder=_('Twój e-mail')),
                css_class='form-group'
            ),
            Div(
                Field('phone', css_class='form-control w-100', placeholder=_('Twój numer telefonu')),
                css_class='form-group'
            ),
            Div(
                Field('date_from', css_class='form-control w-100', placeholder=_('Od')),
                css_class='form-group'
            ),
            Div(
                Field('date_to', css_class='form-control w-100', placeholder=_('Do')),
                css_class='form-group'
            ),
            Div(
                Field('message', css_class='form-control w-100', placeholder=_('Wiadomość')),
                css_class='form-group'
            ),
            Submit('send', _('Wyślij'), css_class='btn btn-primary w-100')
        )

    class Meta:
        fields = ['bike', 'mail', 'phone', 'date_from', 'date_to', 'message']
        # widgets = {
        #     'message': forms.Textarea(attrs={'rows': 2})
        # }

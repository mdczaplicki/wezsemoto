from django.contrib.auth.models import User
from django.db import models
from django.db.models import Max
from django.utils.text import slugify
from django.utils.translation import gettext as _
from ordered_model.models import OrderedModel


class BikeParameters(models.Model):
    name = models.CharField(_('nazwa'), max_length=50)
    capacity = models.CharField(_('pojemność'), max_length=30)
    power = models.CharField(_('moc'), max_length=30)
    torque = models.CharField(_('moment obrotowy'), max_length=30)
    tank_capacity = models.CharField(_('pojemność baku'), max_length=30)
    seat_height = models.CharField(_('wysokość siedzenia'), max_length=30)
    extra = models.CharField(_('dodatkowe wyposażenie'), max_length=255)

    def __str__(self):
        return self.name


class Bike(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('nazwa'))
    brand = models.CharField(max_length=50, verbose_name=_('marka'))
    model = models.CharField(max_length=50, verbose_name=_('model'))
    date_of_production = models.DateField(verbose_name=_('data produkcji'))
    plate_number = models.CharField(max_length=15, verbose_name=_('numer rejestracyjny'))
    is_active = models.BooleanField(_('jest aktywny'), default=True)
    is_visible = models.BooleanField(_('jest widoczny'), default=True)
    slug = models.SlugField()
    short_description = models.TextField(max_length=90, verbose_name=_('krótki opis'), null=True)
    description = models.TextField(max_length=300, verbose_name=_('opis'), null=True)
    bike_parameters = models.ForeignKey(BikeParameters, on_delete=models.SET_NULL, verbose_name=_('parametry'),
                                        null=True)

    def shorten_description(self):
        return self.description[:90] + '...' if len(self.description) > 90 else self.description

    def save(self, *args, **kwargs):
        self.slug = slugify(self.brand) + slugify(self.model)
        super().save(*args, **kwargs)

    def __str__(self):
        # return '%s %s %s' % (self.brand, self.model, self.date_of_production.year)
        return self.name

    class Meta:
        verbose_name = _('motocykl')
        verbose_name_plural = _('motocykle')


class BikePhoto(OrderedModel):
    photo = models.ImageField(verbose_name=_('zdjęcie'), upload_to='bikes')
    bike = models.ForeignKey(Bike, on_delete=models.CASCADE, verbose_name=_('motocykl'))
    order_with_respect_to = 'bike'


class Reservation(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, verbose_name=_('użytkownik'), null=True)
    bike = models.ForeignKey(Bike, on_delete=models.SET_NULL, verbose_name=_('motocykl'), null=True)
    start_date = models.DateField(_('data rozpoczęcia'))
    end_date = models.DateField(_('data zakończenia'))
    information = models.TextField(_('Dodatkowe informacje'))

    class Meta:
        verbose_name = _('rezerwacja')
        verbose_name_plural = _('rezerwacje')


class BikeOffer(models.Model):
    price = models.DecimalField(_('cena'), max_digits=6, decimal_places=0)
    duration = models.CharField(_('okres'), max_length=255)
    bike = models.ForeignKey(Bike, on_delete=models.CASCADE, verbose_name=_('motocykl'))

    def get_price(self):
        return self.price + ' zł'


class Settings(models.Model):
    key = models.CharField(_('klucz'), max_length=30)
    value = models.CharField(_('wartość'), max_length=500)

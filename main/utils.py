from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, ButtonHolder, Submit, LayoutObject
from crispy_forms.utils import flatatt, TEMPLATE_PACK
from django.template.loader import render_to_string
from django.utils.translation import gettext as _
from django.views import View
from django.views.generic import TemplateView
from django_filters import FilterSet, CharFilter
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin


class PaginatedFilteredTableView(SingleTableMixin, FilterView):
    paginate_by = 10
    new_fab = None
    ordering = ['id']

    def get_table_data(self):
        return self.filterset.qs

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.new_fab is not None:
            data['new'] = self.new_fab
        return data


class GenericFilterFormHelper(FormHelper):
    _form_method = 'GET'
    form_class = 'form form-inline table-filter-form'
    form_show_labels = False
    layout = Layout(
        Field('search', placeholder=_('Szukaj'), wrapper_class='w-100', css_class='w-100 form-control')
    )


class GenericFilter(FilterSet):
    search = CharFilter(field_name=_('szukaj'), method='search_filter', label=_('szukaj'))

    model = None
    form_helper = GenericFilterFormHelper

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.form.helper = self.form_helper()

    class Meta:
        fields = ['search']


class LabelFreeFormHelper(FormHelper):
    def __init__(self, legend: str, fields: list, form=None):
        super().__init__(form=form)
        self.form_show_labels = False
        self.layout = Layout(
            Fieldset(
                legend,
                *[
                    Div(
                        Field(
                            field, css_class='form-control w-100', placeholder=placeholder
                        ), css_class='form-group'
                    )
                    for field, placeholder, *args in fields
                ]
            ),
            Submit('submit', _('Zapisz'), css_class='btn btn-primary w-100')
        )


class Img(LayoutObject):
    template = "admin/crispy/img.html"

    def __init__(self, src, **kwargs):
        self.src = src
        if hasattr(self, 'css_class') and 'css_class' in kwargs:
            self.css_class += ' %s' % kwargs.pop('css_class')
        if not hasattr(self, 'css_class'):
            self.css_class = kwargs.pop('css_class', None)

        self.css_id = kwargs.pop('css_id', '')
        self.template = kwargs.pop('template', self.template)
        self.flat_attrs = flatatt(kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, **kwargs):
        template = self.get_template_name(template_pack)
        return render_to_string(template, {'img': self})

from django.contrib.auth.models import User
from django.core import serializers
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import FormView, CreateView, UpdateView

from main.filters import UserFilter, BikeFilter, BikeParametersFilter
from main.forms import BikeForm, EditBikeForm, EditBikePhotosForm, EditBikeOfferForm, BikeParametersForm, \
    EditBikeParametersForm
from main.models import Bike, BikePhoto, BikeOffer, BikeParameters
from main.tables import AdminUserTable, AdminBikeTable, SmallBikeOfferTable, AdminBikeParametersTable
from main.utils import PaginatedFilteredTableView


# noinspection PyUnusedLocal
def admin_index(request):
    return redirect(reverse_lazy('admin_users'))


class AdminUserListView(PaginatedFilteredTableView):
    model = User
    template_name = 'admin/generic_filter_list.html'
    table_class = AdminUserTable
    filterset_class = UserFilter


class AdminBikeListView(PaginatedFilteredTableView):
    model = Bike
    template_name = 'admin/generic_filter_list.html'
    table_class = AdminBikeTable
    filterset_class = BikeFilter
    new_fab = reverse_lazy('new_bike')


class AdminBikeParametersListView(PaginatedFilteredTableView):
    model = BikeParameters
    template_name = 'admin/generic_filter_list.html'
    table_class = AdminBikeParametersTable
    filterset_class = BikeParametersFilter
    new_fab = reverse_lazy('new_bike_parameters')


class AdminBikeFormView(CreateView):
    template_name = 'admin/generic_form.html'
    form_class = BikeForm
    success_url = reverse_lazy('admin_bikes')

    def post(self, request, *args, **kwargs):
        form = self.get_form(self.form_class)
        files = request.FILES.getlist('images')
        if form.is_valid():
            return self.images_form_valid(form, files)
        else:
            return self.form_invalid(form)

    def images_form_valid(self, form, images):
        self.object = form.save_with_images(images, True)
        return HttpResponseRedirect(self.get_success_url())


class AdminBikeParametersFormView(CreateView):
    template_name = 'admin/generic_form.html'
    form_class = BikeParametersForm
    success_url = reverse_lazy('admin_bike_parameters')


class AdminEditBikeParametersFormView(UpdateView):
    model = BikeParameters
    template_name = 'admin/generic_form.html'
    form_class = EditBikeParametersForm
    success_url = reverse_lazy('edit_bike_parameters')


# noinspection PyAttributeOutsideInit
class AdminEditBikeFormView(UpdateView):
    model = Bike
    template_name = 'admin/edit_bike_form.html'
    form_class = EditBikeForm
    image_form_class = EditBikePhotosForm
    offer_form_class = EditBikeOfferForm
    success_url = reverse_lazy('edit_bike')

    def get(self, request, *args, **kwargs):
        # super().get()
        self.object = self.get_object()
        data = self.get_context_data()
        data['images_form'] = self.get_form(form_class=self.image_form_class)
        data['offer_form'] = self.get_form(form_class=self.offer_form_class)
        data['images'] = self.object.bikephoto_set.all()
        data['offers'] = SmallBikeOfferTable(data=self.object.bikeoffer_set.all())

        return self.render_to_response(data)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if 'add_photo' in request.POST.keys():
            files = request.FILES.getlist('images')
            form = self.get_form(form_class=self.image_form_class)
            return self.images_form_valid(form, files)
        elif 'add_offer' in request.POST.keys():
            form = self.get_form(form_class=self.offer_form_class)
            return self.offer_form_valid(form)
        else:
            form = self.get_form()
            if form.is_valid():
                return self.form_valid(form)
        return self.form_invalid(form)

    def images_form_valid(self, form, images):
        self.object = form.save_with_images(images, True)
        return HttpResponseRedirect(self.get_success_url())

    def offer_form_valid(self, form):
        self.object = self.get_object()
        form.save(bike=self.get_object())
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('edit_bike', args=[self.get_object().pk])


def update_image_positions(request):
    if request.POST:
        items = request.POST.getlist('items[]')
        for position, item in enumerate(items):
            bp = BikePhoto.objects.get(id=item)
            bp.bottom()
            bp.save()
        return JsonResponse({'result': 1})


def delete_image(request):
    if request.POST:
        bike_photo_id = request.POST['id']
        BikePhoto.objects.get(id=bike_photo_id).delete()
        return JsonResponse({'result': 1})


def delete_offer(request):
    if request.POST:
        bike_offer_id = request.POST['id']
        BikeOffer.objects.get(id=bike_offer_id).delete()
        return JsonResponse({'result': 1})


def get_bike_parameters(request):
    if request.method == 'GET':
        bp = BikeParameters.objects.get(id=request.GET['pk'])
        json = serializers.serialize('json', [bp, ], fields=['capacity', 'power', 'torque', 'tank_capacity',
                                                             'seat_height', 'extra'])
        return HttpResponse(json, content_type='application/json')

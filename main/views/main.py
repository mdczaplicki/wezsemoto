from django.shortcuts import render
from django.urls import reverse_lazy

from django.utils.translation import gettext as _
from django.views import View
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import FormMixin

from main.forms import BikeContactForm
from main.models import Bike


def index(request):
    return render(request, 'index.html', {
        'title_appendix': _('Strona główna'),
        'active_nav': 'index'
    })


class BikeListView(ListView):
    template_name = 'bike_list.html'
    queryset = Bike.objects.all()
    extra_context = {
        'active_nav': 'motorcycle_list',
        'title_appendix': _('Motocykle')
    }


class BikeDetailsView(DetailView, FormMixin):
    template_name = 'bike_details.html'
    model = Bike
    pk_url_kwarg = 'id'
    form_class = BikeContactForm
    success_url = reverse_lazy('bike_list')

    # def get_context_data(self, **kwargs):
    #     data = super().get_context_data(**kwargs)
    #     data['contact_form'] = BikeContactForm
    #     return data

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(initial={'bike': self.get_object()})

from django.urls import path

from main.views import admin_index, AdminUserListView, AdminBikeListView, AdminBikeFormView, AdminEditBikeFormView, \
    update_image_positions, delete_image, delete_offer, AdminBikeParametersListView, AdminBikeParametersFormView, \
    AdminEditBikeParametersFormView, get_bike_parameters

urlpatterns = [
    path('', admin_index, name='admin'),
    path('users/', AdminUserListView.as_view(), name='admin_users'),
    path('bikes/', AdminBikeListView.as_view(), name='admin_bikes'),
    path('bike_parameters/', AdminBikeParametersListView.as_view(), name='admin_bike_parameters'),
    path('new_bike/', AdminBikeFormView.as_view(), name='new_bike'),
    path('new_bike_parameters/', AdminBikeParametersFormView.as_view(), name='new_bike_parameters'),
    path('edit_bike/<int:pk>', AdminEditBikeFormView.as_view(), name='edit_bike'),
    path('edit_bike_parameters/<int:pk>', AdminEditBikeParametersFormView.as_view(), name='edit_bike_parameters'),
    path('update_image_positions', update_image_positions, name='update_image_positions'),
    path('delete_image', delete_image, name='delete_image'),
    path('delete_offer', delete_offer, name='delete_offer'),
    path('get_bike_parameters/', get_bike_parameters, name='get_bike_parameters'),
]

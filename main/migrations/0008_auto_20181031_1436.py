# Generated by Django 2.1.2 on 2018-10-31 13:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_bikeoffer_bike'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bikeoffer',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=6, verbose_name='cena'),
        ),
    ]

# Generated by Django 2.1.2 on 2019-01-14 13:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_auto_20190114_1406'),
    ]

    operations = [
        migrations.AddField(
            model_name='bikeparameters',
            name='name',
            field=models.CharField(default='', max_length=50, verbose_name='nazwa'),
            preserve_default=False,
        ),
    ]

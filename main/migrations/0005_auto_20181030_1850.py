# Generated by Django 2.1.2 on 2018-10-30 17:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20181023_1621'),
    ]

    operations = [
        migrations.AddField(
            model_name='bikephoto',
            name='position',
            field=models.IntegerField(null=True, verbose_name='Pozycja'),
        ),
        migrations.AlterUniqueTogether(
            name='bikephoto',
            unique_together={('position', 'bike')},
        ),
    ]

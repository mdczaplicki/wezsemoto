from django.db.models import Q

from main.utils import GenericFilter


class UserFilter(GenericFilter):
    def search_filter(self, queryset, name, value):
        for word in value.split():
            queryset = queryset.filter(
                Q(first_name__icontains=word) |
                Q(username__icontains=word) |
                Q(last_name__icontains=word) |
                Q(email__icontains=word)
            )
        return queryset


class BikeFilter(GenericFilter):
    def search_filter(self, queryset, name, value):
        for word in value.split():
            queryset = queryset.filter(
                Q(brand__icontains=word) |
                Q(model__icontains=word) |
                Q(date_of_production__year__icontains=word) |
                Q(plate_number__icontains=word)
            )
        return queryset


class BikeParametersFilter(GenericFilter):
    def search_filter(self, queryset, name, value):
        for word in value.split():
            queryset = queryset.filter(
                Q(name__icontains=word)
            )
        return queryset

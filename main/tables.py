from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import gettext as _
from django_tables2 import Table, columns

from main.models import Bike, BikeOffer, BikeParameters


class AdminUserTable(Table):
    class Meta:
        model = User
        exclude = ('password',)
        sequence = ('id', 'last_name', 'first_name', 'username', 'email', 'is_active')
        attrs = {'class': 'table table-striped'}
        order_by = 'id'


# noinspection PyMethodMayBeStatic
class AdminBikeParametersTable(Table):
    name = columns.Column(empty_values=())

    class Meta:
        model = BikeParameters
        attrs = {'class': 'table table-striped'}
        order_by = 'id'

    def render_name(self, record: BikeParameters):
        data = {
            'href': reverse('edit_bike_parameters', args=[record.id]),
            'value': record.name
        }
        return format_html(render_to_string('admin/cells/generic_a_cell.html', data))


# noinspection PyMethodMayBeStatic
class AdminBikeTable(Table):
    photos_count = columns.Column(verbose_name=_('Ilość zdjęć'), empty_values=())

    def render_photos_count(self, record: Bike):
        return format_html('{0}', record.bikephoto_set.count())

    def render_plate_number(self, record: Bike):
        data = {
            'href': reverse('edit_bike', args=[record.id]),
            'value': record.plate_number
        }
        return format_html(render_to_string('admin/cells/generic_a_cell.html', data))

    class Meta:
        model = Bike
        attrs = {'class': 'table table-striped table-hover'}
        exclude = ('description',)
        order_by = 'id'


# noinspection PyMethodMayBeStatic
class SmallBikeOfferTable(Table):
    action = columns.Column(verbose_name='', empty_values=())

    def render_action(self, record: BikeOffer):
        return format_html(render_to_string('admin/cells/offer_action.html', {'id': record.id}))

    class Meta:
        model = BikeOffer
        exclude = ('id', 'bike')
        sequence = ('duration', 'price')
        attrs = {'class': 'table offer-table'}

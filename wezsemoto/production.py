from .settings import *

DEBUG = False

ALLOWED_HOSTS = []

COMPRESS_STORAGE = 'compressor.storage.GzipCompressorFileStorage'
COMPRESS_OFFLINE = True

from django.conf.urls.static import static
from django.urls import path, include

from main.views import index, AdminBikeListView, BikeDetailsView, BikeListView
from wezsemoto import settings

urlpatterns = [
    path('', index, name='index'),
    path('motocykle', BikeListView.as_view(), name='bike_list'),
    path('motocykl/<int:id>', BikeDetailsView.as_view(), name='bike_details'),
    path('motocykl/<int:id>/<str:slug>', BikeDetailsView.as_view(), name='bike_details'),
    path('admin/', include('main.urls.admin'))
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)